![Bild 1](media/1.jpeg)

**När Värgårdsskolan i Kisa byggdes om och till fanns BIM med från start**

# Kraftigt minskat antal ätor i BIM-baserat skolprojekt

> ##### BIM-baserad metodik från start, rejäl satsning på projekteringen, kvalitetskontroller och visualiseringar, hög andel brukarmedverkan och väl fungerande kommunikation mellan alla inblandade parter – allt har bidragit till att om- och tillbyggnaden av Värgårdsskolan i Kisa blivit ett lyckat projekt. Och inte minst – mängden avvikelser var under tre procent jämfört med normala tio procent!

PROJEKTLEDARE FREDRIK WIRF FRÅN WSP var tidigt inkopplad i arbetet med att bygga om och till Värgårdsskolan, en högstadieskola i Kisa i Kinda kommun. I diskussionerna kring hur projektet skulle läggas upp var byggherre och brukare överens med honom om att lägga tyngdpunkt på projekteringen för att få bättre kontroll och ekonomistyrning och därmed undvika senare överraskningar. Kontentan av detta blev att det tidigt bestämdes att det skulle göras en BIM-projektering.
​	– Jag drev BIM-inriktningen med de övrigas goda minne, säger Fredrik Wirf. Jag hade sett andra skolprojekt med för snabba projekteringar och upphandlingar med stora kostnadsökningar som följd. Det är bättre att lägga mer pengar på projekteringen och få det rätt från början. 
​	Vid upphandlingen av konsultgruppen var kravet att allt arbete skulle vara BIM-baserat. För Fredrik Wirf var 3D och alla därtill hörande programvaror inga konstigheter men han hade inte tidigare arbetat praktiskt med BIM-projektering.
BIM-konceptet – att modellen ska vara ledande, att objekten ska innehålla mycket information och vara kodade för ekonomi, modellsamordning med mängdavttagningar och mycket annat – var 2011 nytt för de allra flesta som deltog i projektet och därför krävdes inledningsvis mycket utbildning och arbete med att hitta de rätta formerna för arbetet.
​	– Det tog ett tag att få alla med på banan, många fastnade i traditionella värderingar och hänvisade till att så här har vi alltid gjort. Insikten att det inte räcker med nya program utan att man även måste ha en annan metodik, satt långt inne för vissa projektörer.
​	Följden blev att mycket arbete hamnade hos Fredrik Wirf. Som projektledare, projekteringsledare och BIM-samordnare hade han ett helhetsgrepp och näst intill total kontroll på projektet. Att det innebar mycket arbete ser han inte som
någon nackdel men för att upplägget ska fungera förutsätts att projektet inte är för stort. Skolans rektor Johannes Kullered, som deltog i projekteringen
från början, utbildade sig i programvaran Solibri, vilket innebar att han enkelt kunde ladda hem och följa modellen och visa den för sina lärargrupper.
​	– Han var verkligen intresserad av detta. Det krävs att någon
vill lägga den tid som behövs och själv visa modellen, då kan det bli riktigt bra.
​	Brukarnas engagemang och vilja att vara med praktiskt i processen innebar att Fredrik Wirf och hans kollegor fick många goda synpunkter och att projekteringen fick en bra förankring från början. Efter projekteringen har det inte heller kommit några krav på omändringar.